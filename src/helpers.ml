open Core
open Yojson.Basic.Util

type step = {read: char; write: char; to_state: string; action: string}
type state = {name: string; steps: step list}
type turing = {name: string; possible_states: string list; initial: string; states: state list; finals: string list; alphabet: char list; tape: char array}

let step_new ~rd:nread ~wr:nwrite ~st:nstate ~at:naction = {read = nread; write = nwrite; to_state = nstate; action = naction}
let state_new n ~steps:step = {name = n; steps = step}

let exit_error msg = printf "%s\n" msg; exit 2

let find_or_die x ~f:f ?(error = "An error occured") = match (List.find x ~f:f) with
  | None -> printf "%s\n" error; exit 1
  | Some a -> a

let rec find_key_or_die list ~key:k' = match list with
  | [] -> exit_error (String.concat ["Missing key: "; k'])
  | (k, v) :: t -> if (String.equal k k') then v else find_key_or_die t ~key:k'

let to_step tuple = step_new ~rd:(find_key_or_die tuple ~key:"read").[0] ~wr:(find_key_or_die tuple ~key:"write").[0] ~st:(find_key_or_die tuple ~key:"to_state") ~at:(find_key_or_die tuple ~key:"action")
let to_state transitions = List.map transitions ~f:(function x -> match x with (name, steps) -> state_new name ~steps:(List.map steps ~f:to_step))

let print_tape tape head = 
  printf "[";
  Array.iteri ~f:(fun x y -> printf (if head = x then "<%c>" else "%c") y) tape;
  printf "]"

let print_inst_short name ~read:tape = printf "(%s, %c)" name tape
let print_inst_long name ~read:tape ~next:next = printf "(%s, %c, %s)" name tape next

let print_transition now ~name:name =
  print_inst_short name ~read:now.read;
  printf " -> ";
  print_inst_long now.to_state ~read:now.write ~next:now.action

let print_algo_step head ~tape:tape ~now:now ~cname:cname ~next:next =
  print_tape tape head;
  printf " ";
  print_transition now ~name:cname;
  printf "\n"

let print_instruction x = 
  printf "------------------\n";
  List.iter x ~f:(fun x -> match x with (k, v) -> printf "%s: %s\n" k (v |> to_string));
  printf "------------------\n"

let rec find_algo list char = match list with
  | [] -> exit 1
  | a :: t -> if (List.exists ~f:(fun x -> match x with (a, b) -> String.equal a "read" && b = char)) a then a else find_algo t char

let dir_to_int s = match s with
  | "RIGHT" -> +1
  | "LEFT" -> -1
  | _ -> exit 1

let rec interprete ~machine:machine ?(counter=0) current_state head  = 
  if head < 0 then
    printf "Head fell off the tape!\n"
  else begin (if List.exists machine.finals ~f:(String.equal current_state) then begin
                printf "Machine completition successfull!\nTime complexity: %d steps!\n" counter;
              end
              else begin
                let cstate = find_or_die machine.states ~f:(fun x -> String.equal x.name current_state) ~error:(String.concat ["Cannot find state "; current_state]) in
                let cstep = find_or_die cstate.steps ~f:(fun x -> Char.equal x.read machine.tape.(head)) ~error:(String.concat ["Cannot find an action in "; current_state; " for "; String.make 1 machine.tape.(head)]) in
                Array.set machine.tape head cstep.write;
                print_algo_step head ~cname:current_state ~tape:machine.tape ~now:cstep ~next:current_state;
                interprete ~machine:machine cstep.to_state (head + dir_to_int cstep.action) ~counter:(counter + 1)
              end);
  end
