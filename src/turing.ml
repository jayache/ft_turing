open Core
open Yojson.Basic.Util
INCLUDE "src/helpers.ml"

let parsed = 
  let usage_msg = "usage: ft_turing [-h] jsonfile input

positional arguments:
 jsonfile           json description of the machine

 input              input of the machine

optional arguments:
 -h, --help         show this help message and exit" in

  let argv = Core.Sys.get_argv () in

  let arg_exists x = not (String.equal (Option.value (Array.find argv ~f:(String.equal x)) ~default:"zero") "zero") in
  if arg_exists "-h" || arg_exists "--help" || (Array.length argv) <> 3  then begin
    printf "%s\n" usage_msg;
    exit 0;
  end;

  let machine = argv.(1) in
  let tape_arg = argv.(2) in
  let json = 
    try  
      Yojson.Basic.from_file machine
    with 
    | _ -> exit_error "Invalid JSON provided.\n" in
  let tape_base = Array.create ~len:60 (json |> member "blank" |> to_string) in
  let tape = Array.mapi tape_base ~f:(fun i x -> if i < String.length tape_arg then tape_arg.[i] else x.[0]) in
  let alphabet = json |> member "alphabet" |> to_list |> List.map ~f:(fun x -> x |> to_string |> String.fold ~init:' ' ~f:(fun x y -> y)) in
  Array.iter ~f:(fun x -> if not (List.exists alphabet ~f:(fun y -> Char.equal y x)) then exit_error (String.concat ["Tape contains illegal characters:"; String.make 1 x; "!\n"])) tape;
  let finals = json |> member "finals" |> to_list |> List.map ~f:to_string in
  let transitions = json |> member "transitions" |> to_assoc |> List.map ~f:(function x -> match x with 
      | (key, l) -> (key, List.map (l |> to_list) ~f:(fun x -> List.map (x |> to_assoc) ~f:(fun x -> match x with
          | (key, va) -> (key, va |> to_string))))) in

  let program = List.map (json |> member "states" |> to_list) ~f:(fun x -> x |> to_string) in
  let name = json |> member "name" |> to_string in
  let initial = json |> member "initial" |> to_string in
  {possible_states = program; name = name; initial = initial; states = to_state transitions; finals= finals; alphabet = alphabet; tape = tape} in

let machine = parsed in

printf "%s\n" (String.make 60 '*');
printf "*%s*\n" (String.make 58 ' ');
printf "*%30s%28s*\n" machine.name "";
printf "*%s*\n" (String.make 58 ' ');
printf "%s\n" (String.make 60 '*');
printf "Alphabet: [%s]\n" (String.concat (machine.alphabet |> List.map ~f:(String.make 1)) ~sep:", ");
printf "States : [%s]\n" (String.concat machine.possible_states ~sep:", ");
printf "Finals : [%s]\n" (String.concat machine.finals ~sep:", ");
printf "Initial: %s\n" machine.initial;

let program = machine.states in

List.iter program ~f:(fun x -> printf "%s\n" x.name);
List.iter program ~f:(fun x ->
    List.iter x.steps ~f:(fun y ->
        print_transition y ~name:x.name;
        printf "\n";
      )
    );
printf "%s\n" (String.make 60 '*');

interprete ~machine:machine machine.initial 0 ~counter:0;
printf "[";
Array.iter ~f:(printf "%c") machine.tape;
printf "]\n";
