TARGET=ft_turing

SRC_FILE=helpers.ml turing.ml
SRC_DIR=src/
SRCS=$(addprefix $(SRC_DIR), $(SRC_FILE))

OBJ_DIR=obj/
OBJ_FILE=$(SRC_FILE:.ml=.cmx)
OBJS=$(addprefix $(OBJ_DIR), $(OBJ_FILE))

all: $(TARGET)
	@echo "Everything's done!"

packages:
	@opam --version || brew install opam
	@if [ -z "$(opam info core | grep installed)" ]; then echo Already installed: core; else opam install core; fi
	@if [ -z "$(opam info yojson | grep installed)" ]; then echo Already installed: yojson; else opam install core; fi
	@if [ -z "$(opam info camlp4 | grep installed)" ]; then echo Already installed: camlp4; else opam install core; fi
	
$(OBJ_DIR)%.cmx: $(SRC_DIR)%.ml	 | packages
	ocamlfind ocamlopt -o $@ -syntax camlp4o -package camlp4 -ppopt pa_macro.cmo  -linkpkg -package core -package yojson -c $<

$(TARGET): $(OBJS)
	echo $(OBJS)
	ocamlfind ocamlopt -o $(TARGET) -linkpkg -package core -package yojson $(OBJS)

clean:
	rm $(OBJS)

fclean: clean
	rm $(TARGET)

re: fclean all
