template = '
{
  "name": "meta_unary_add",
  "alphabet": [ ALPHABET ],
  "blank" : ".",
  "states" : [ RUN_STATES ],
  "initial" : "init",
  "finals" : [ HALT_STATES ],
  "transitions" : {'

END_STATE = "    ],\n"
END_JSON = "    ]\n}\n}"

def generate_step(read, write, state, direction, comma = true)
  "\t{\"read\": #{read.inspect}, \"write\": #{write.inspect}, \"to_state\": #{state.inspect}, \"action\": #{direction.inspect}}#{"," if comma}"
end

def generate_skip_for(alphabet, state, direction)
  alphabet.map { |a| generate_step(a, a, state, direction, false) }.join ",\n"
end

def generate_state_name(name)
  "\t" + name.inspect + ": ["
end

def generate_parametized_step(alphabet, write, state_prefix, direction)
  ALLOWED.map { |a| generate_step(a, write, state_prefix + a, direction, false)}.join ",\n"
end

def generate_parametized_read(alphabet, state_prefix, direction)
  ALLOWED.map { |a| generate_step(a, a, state_prefix + a, direction, false)}.join ",\n"
end

NORMAL_STATES = %w{init read_char go_empty read_empty mark_current read_action set_action_R set_action_L go_to_current_step skip_to_write skip_to_state skip_to_state_2 read_write read_state mark_state go_to_start}
META_STATES = %w{go_to_tape_ go_step_ is_step_ find_step_ go_current_state_ is_state_ find_state_}
RESERVED = [';', ':', 'L', 'R', '$', '@', '#', '!', '>', '<', '.', '%', '|']
ALLOWED = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', '1', '2', '3']
ALL_CHAR = RESERVED + ALLOWED
ALL_META_STATES = ALLOWED.collect {|c| META_STATES.collect {|s| s + c } }.flatten
HALT_COMPLETION_SUCCESSFULL = "HALT: COMPLETION SUCCESSFULL"
HALT_STATES = ["HALT: NO VALID STATES", "HALT: COMPLETION SUCCESSFULL", "HALT: NO VALID STEP", "HALT: WTF??!"]

template.gsub!(/ALPHABET/, (ALLOWED + RESERVED).map(&:inspect).join(", "))
template.gsub!(/RUN_STATES/, (NORMAL_STATES+ ALL_META_STATES).map(&:inspect).join(", "))
template.gsub!(/HALT_STATES/, (HALT_STATES).map(&:inspect).join(", "))

puts template

puts generate_state_name("init")
puts generate_step("@", ".", "read_char", "RIGHT")
puts generate_skip_for((ALLOWED + RESERVED).reject {|a| a == '@'}, "init", "RIGHT")
puts END_STATE

puts generate_state_name("read_char")
puts generate_step(".", "@", "read_empty", "LEFT")
puts generate_parametized_step(ALLOWED, "@", "go_current_state_", "LEFT")
puts END_STATE

puts generate_state_name("go_empty")
puts generate_step('>', '>', "read_empty", "RIGHT")
puts generate_skip_for(ALL_CHAR.reject {|a| a == '>'}, "go_empty", "RIGHT")
puts END_STATE

puts generate_state_name("read_empty")
puts generate_parametized_read(ALLOWED, "go_to_tape_", "RIGHT")
puts END_STATE

puts generate_state_name("mark_current")
puts generate_step(";", ":", "read_action", "RIGHT")
puts generate_skip_for(ALL_CHAR.reject {|a| a == ";"}, "HALT: WTF??!", "RIGHT")
puts END_STATE

puts generate_state_name("read_action")
puts generate_step("R", "R", "set_action_R", "RIGHT")
puts generate_step("L", "R", "set_action_L", "RIGHT")
puts generate_skip_for(ALLOWED, "read_action", "RIGHT")
puts END_STATE

puts generate_state_name("set_action_R")
puts generate_step("@", ">", "go_to_current_step", "LEFT")
puts generate_skip_for(ALL_CHAR.reject{|a| a == '@'}, "set_action_R", "RIGHT")
puts END_STATE

puts generate_state_name("set_action_L")
puts generate_step("@", "<", "go_to_current_step", "LEFT")
puts generate_skip_for(ALL_CHAR.reject{|a| a == '@'}, "set_action_L", "RIGHT")
puts END_STATE

puts generate_state_name("go_to_current_step")
puts generate_step(":", "!", "skip_to_state", "RIGHT")
puts generate_step("!", ";", "skip_to_write", "RIGHT")
puts generate_skip_for(ALL_CHAR.reject{|a| [':', '!'].include? a}, "go_to_current_step", "LEFT")
puts END_STATE

puts generate_state_name("skip_to_write")
puts generate_skip_for(ALLOWED, "read_write", "RIGHT")
puts END_STATE

puts generate_state_name("skip_to_state")
puts generate_skip_for(ALLOWED, "skip_to_state_2", "RIGHT")
puts END_STATE

puts generate_state_name("skip_to_state_2")
puts generate_skip_for(ALLOWED, "read_state", "RIGHT")
puts END_STATE

puts generate_state_name("read_write")
puts generate_parametized_read(ALLOWED, "go_to_current_tape_", "RIGHT")
puts END_STATE

puts generate_state_name("read_state")
puts generate_parametized_read(ALLOWED, "go_to_start_", "LEFT")
puts END_STATE

puts generate_state_name("mark_state")
puts generate_step("#", "$", "go_to_start", "LEFT", false)
puts END_STATE

puts generate_state_name("go_to_tape")
puts generate_step(">", ">", "go_to_current_step", "LEFT")
puts generate_step("<", "<", "go_to_current_step", "LEFT")
puts generate_skip_for(ALL_CHAR.reject {|a| ["<", "<"].include? a}, "go_to_tape", "RIGHT")
puts END_STATE

ALLOWED.each do |name|
  puts generate_state_name("go_to_tape_#{name}")
  puts generate_step("@", "@", "go_current_state_#{name}", "LEFT")
  puts generate_skip_for(ALL_CHAR.reject {|a| a == "@"}, "go_to_tape_#{name}", "RIGHT")
  puts END_STATE

  puts generate_state_name("go_current_state_#{name}")
  puts generate_step("$", "#", "go_step_#{name}", "RIGHT")
  puts generate_step(">", ">", "HALT: NO VALID STATES", "RIGHT")
  puts generate_step("%", "%", "write_before_exiting_#{name}", "RIGHT")
  puts generate_skip_for(ALL_CHAR.reject {|a| ['$', '>', '%'].include? a}, "go_current_state_#{name}", "LEFT")
  puts END_STATE

  puts generate_state_name("go_step_#{name}")
  puts generate_step(";", ";", "is_#{name}", "RIGHT")
  puts generate_step("#", "#", "HALT: NO VALID STEP", "RIGHT")
  puts generate_skip_for(ALL_CHAR.reject {|a| [';', '#'].include? a}, "go_step_#{name}", "RIGHT")
  puts END_STATE
  
  puts generate_state_name("is_#{name}")
  puts generate_step(name, name, "mark_current", "LEFT")
  puts generate_skip_for(ALLOWED.reject {|a| a == name}, "skip_one_step_#{name}", "LEFT")
  puts END_STATE

  puts generate_state_name("skip_one_step_#{name}")
  puts generate_skip_for(ALL_CHAR, "go_step_#{name}", "RIGHT")
  puts END_STATE

  puts generate_state_name("go_to_start_#{name}")
  puts generate_step(">", ">", "skip_one_char_#{name}", "RIGHT")
  puts generate_skip_for(ALL_CHAR.reject {|a| a == ">"}, "go_to_start_#{name}", "LEFT")
  puts END_STATE

  puts generate_state_name("skip_one_char_#{name}")
  puts generate_skip_for(ALL_CHAR, "match_halt_state_#{name}", "RIGHT")
  puts END_STATE

  puts generate_state_name("match_halt_state_#{name}")
  puts generate_step("|", "|", "search_state_#{name}", "RIGHT")
  puts generate_step(name, "%", "go_to_start", "RIGHT")
  puts generate_skip_for(ALLOWED.reject {|a| a == name}, "match_halt_state_#{name}", "RIGHT")
  puts END_STATE

  puts generate_state_name("search_state_#{name}")
  puts generate_step("#", "#", "is_state_#{name}", "LEFT")
  puts generate_skip_for(ALL_CHAR.reject {|a| a == "#"}, "search_state_#{name}", "RIGHT")
  puts END_STATE

  puts generate_state_name("is_state_#{name}")
  puts generate_step(name, name, "mark_state", "RIGHT")
  puts generate_skip_for(ALLOWED.reject {|a| a == name}, "skip_one_state_#{name}", "RIGHT")
  puts END_STATE

  puts generate_state_name("go_to_current_tape_#{name}")
  puts generate_step(">", name, "read_char", "RIGHT")
  puts generate_step("<", name, "read_char", "LEFT")
  puts generate_skip_for(ALL_CHAR.reject {|a| ['>', '<'].include? a}, "go_to_current_tape_#{name}", "RIGHT")
  puts END_STATE

  puts generate_state_name("skip_one_state_#{name}")
  puts generate_skip_for(ALL_CHAR, "search_state_#{name}", "RIGHT")
  puts END_STATE

  puts generate_state_name("write_before_exiting_#{name}")
  puts generate_step("@", name, HALT_COMPLETION_SUCCESSFULL, "LEFT")
  puts generate_skip_for(ALL_CHAR.reject {|a| a == "@"}, "write_before_exiting_#{name}", "RIGHT")
  puts END_STATE
end
puts generate_state_name("go_to_start")
puts generate_step(">", ">", "go_to_tape", "RIGHT")
puts generate_skip_for(ALL_CHAR.reject {|a| a == ">"}, "go_to_start", "LEFT")
puts END_JSON
