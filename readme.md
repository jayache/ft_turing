# Syntax

## Description
  - name : one character that is not reserved
  - read : one character that is not reserved
  - write : one character that is not reserved
  - blank : one character that is not reserved
  - halt : a valid state name
  - input : arbitrary length, cannot contain reserved characters
  - to_state : a valid state name
  - action : one of L or R
  - state_marker : one of `$` (if start state) or `#`
  - step : `;` read write to_state action
  - state : name state_marker step "[step...]"
  - syntax : `>` blank halt [halt...] `|` state [state...] `@` input

## Reserved Characters
  - `#`: symbol for state
  - `$`: symbol for current state
  - `@`: symbol for current tape pos
  - `;`: symbol for step
  - `:`: symbol for current step
  - `!`: symbol for current step (bis)
  - `>`: symbol for beginning of state, and R action marker
  - `<`: symbol for L action marker
  - `|`: symbol for starting transition section

## Allowed Characters

Any single character in the range [a-z][0-9].

## Example:

  - unary_add: `>oh|a$;ooaR;11aR;b1aR;cobLb#;1ohR;oohR@11b11c`

1$;abL1;baL2;2#aaL1;bbR2

# Diagrams
How to read diagrams:

Boxes are states. Arrows are steps, the destination of the arrow being the resulting state.
On the arrow, the first character is what is read. If there is a ¬ before the character, it means
everything EXCEPT this character. The character after the arrow is what is written instead, 
if there is no arrow then you just write what you read. The last character is which direction the head moves (Left/Right)

∀ matches everything (with lower priority than more specific options).
∃ matches every *allowed* character. That is, not special characters.
δ is a single character that was read prior. Think of it like templates in CPP. 
If a state without δ goes to a state with a δ, the δ is the character that was just read.
If a state with a δ goes to a state with a δ, the δ is conserved between the 2.
## High level description
<p>
<details>
<summary>Click to show</summary>


```mermaid
flowchart TD
A[init] --> B[read char and put marker]
B --> C[go to current state X and remove $]
C --> D[find step X]
D --> E[mark step]
E --> I[read action]
I -- L --> J[go to current tape and put <]
I -- R --> K[go to current tape and put >]
J --> M[go to current step bis]
K --> M
M --> N[read state]
N --> O[mark state as current]
O --> P[go to current step thris]
P --> Q[delete current marker on step]
Q --> S[read what to write]
S --> T[go to current tape X]
T --> U[write char and move according to char]
U --> B

```

</details>
</p>

## States:

<p>
<details>
<summary>Click to show</summary>

```mermaid
flowchart TD
A[init] -- "@→."R --> B[read_char]
A -- "¬@"R --> A
B -- ".→@"L --> Z[go_empty]
Z -- ">"R --> AD[read_empty]
AD -- "∃"R --> AA[go_to_tape_δ]
AA -- "@"L --> C
AA -- "¬@"R --> AA
Z -- "¬>"L --> Z
B -- "∃→@"L --> C[go_current_state_δ]
C -- "$→;"R --> G[go_step_δ]
C -- ">"R --> ZZ[HALT_ERROR_NO_STATE]
C -- "%"R --> AG[write_before_exiting_δ]
AG -- "¬@"R --> AG
AG -- "@→δ"L --> ZA[HALT_COMPLETION_SUCCESSFULL]
C -- "¬$"L --> C
G -- ";"R --> D[is_δ]
G -- "#"R --> ZY[HALT_ERROR_NO_STEP]
G -- "¬#"R --> G
D -- "δ"L --> E[mark_current]
D -- "¬δ"L --> F[skip_one_step_δ]
F -- "∀"L --> G
E -- ";→:"R --> H[read_action]
E -- "¬;"R --> ZX[HALT_WTF]
H -- "R"R --> I[set_action_R]
H -- "L"R --> J[set_action_L]
H -- "∃"R --> H
I -- "¬@"R --> I
J -- "¬@"R --> J
I -- "@→>"L --> K[go_to_current_step] 
J -- "@→"< L --> K
K -- ":→!"R --> M[skip_to_state]
K -- "!→;"R --> W[skip_to_write]
K -- "¬:"L --> K
M -- "∀"R --> N[skip_to_state_2]
N -- "∀"R --> O[read_state]
O -- "∀"L --> P[go_to_start_δ]
P -- ">"R --> AB[skip_one_char_δ]
P -- "¬>"L --> P
Q[search_state_δ] -- "#"L --> S[is_state_δ]
AB -- "∀"R --> AC[match_halt_state_δ]
AC -- "|"R --> Q
AC -- "δ→%"R --> V
AC -- "¬#"R --> AC
Q -- "¬#"R --> Q
S -- "δ"R --> U[mark_state]
S -- "¬δ"R --> T[skip_one_state_δ]
T -- "∀"R --> Q
U -- "#→$"L --> V[go_to_start]
V -- ">"R --> AK[go_to_tape]
AK -- ">"L --> K
AK -- "< "L --> K
AK -- "¬<"R --> AK
V -- "¬>"L --> V
W -- "∀"R --> X[read_write]
X -- "∃"R --> Y[go_to_current_tape_δ]
Y -- ">→δ"R --> B
Y -- "<→δ"L --> B
Y -- "¬>"R --> Y


```

</details>
</p>
